# Checks and Cooldowns

In this section I'll be covering two important features of `discord.py`'s command framework which help quite a lot with the checking of certain conditions on command invokation. These are checks and cooldowns.

## Checks 

This is an easy way to perform checks before the command function even runs. You can make your own checks, or make use of the wide pallete of built-in checks that the library offers. You can check the full list of all the checks that already come with the command framework [**here**](https://discordpy.readthedocs.io/en/latest/ext/commands/api.html#checks). I'll be focusing on how you can create your own checks though.

Adding checks to a command is done by decorating the command function with the check decorator, either above or bellow the command decorator.  
All a check does is take a command context and return a boolean value, so the first step to create a new custom one is writing a function that takes a Context argument and returns a bool. `discord.py` then provides a method to wrap that function into something more usable by the library: `commands.check`.

Here's a full example of a dummy command that only answers the owner of the guild it's invoked in.
```python
def is_guild_owner(ctx):
    return ctx.guild.owner == ctx.author

@bot.command(name="hi")
@commands.check(is_guild_owner)
async def baz_command(ctx):
    await ctx.send("Hello there!")
```
Trying to invoke this command while not being the guild owner will result in a `CheckFailure` exception being raised.

There's another way of applying checks, however. You can wrap the function with the decorator and then use it directly. Like this:
```python
def is_guild_owner():
    def decorator(ctx):
        return ctx.guild.owner == ctx.author

    return commands.check(decorator)

@bot.command(name="hi")
@is_guild_owner()
async def baz_command(ctx):
    await ctx.send("Hello there!")
```
This code will poduce the same behaviour as the previous one.

## Cooldowns

Cooldowns are a neat way of preventing commands from being spammed and they are essentially checks, just managed my the framework. It's really easy to apply them, all you got to do is decorate the command function with the cooldown decorator *generator*, similarly to "normal" checks. This takes three arguments though: 

* `rate` - This specifies how many invokations can occur within the `per` time window, in seconds. Needs to be an int.
* `per` - The time period in which `rate` invokation can occur at max. Can either be an int or a float.
* `bucket` - Where to apply the cooldown. There are many buckets to chose from. They are listed [**here**](https://discordpy.readthedocs.io/en/latest/ext/commands/api.html#discord.ext.commands.cooldown).

Here's an example:
```python
@bot.command(name="myid")
@commands.cooldown(rate=1, per=5, bucket=commands.BucketType.user)
async def id_inspection_command(ctx):
    await ctx.send(f"Here's your Discord ID: `{ctx.author.id}`")
```
In this case, each user can only invoke the command once every 5 seconds. If you try to invoke while it's on cooldown, then a `CommandOnCooldow` exception is raised.

---

That's it for this section! Have fun messing around with checks and cooldowns and I'll see you on [**Command Groups**](./command-groups.html), where I teach you how you can nest your commands into tidier packs. ***o/***
