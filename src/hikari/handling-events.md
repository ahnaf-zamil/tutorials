# Handling Events

So, we have our bot running and now we want it to do some stuff. For doing that, we have to understand how the Discord API communicates with connected clients. Basically, there are two ways information is transferred between the Discord API and the client - the Gateway and the REST API. The Gateway API is used by Discord to send information about new things that are happening, such as when someone sends a message, a member leaves the server, etc, and the REST API is used by the bot to send data or information over to Discord. In this section, I'll be going over how to handle gateway events in _Hikari_.

We can use _Hikari_'s event listener to add some functionality to the bot when some specific event happens.

Let's see some example code on how to do it!

`bot.py`

```python

import lightbulb
import hikari

with open("./token.txt") as fp:
    TOKEN = fp.read().strip()

bot = lightbulb.Bot(token=TOKEN, prefix="b!")

@bot.listen(hikari.StartedEvent)
async def on_bot_started(event):
    """Prints out 'I am online' when the bot is ready to work"""
    print("I am online")


# Never forget to leave this on the bottom of your code
# Anything after this line will only be executed once the bot logs out
bot.run()
```

_**Note**: All event handlers should be coroutine functions (`async def`)!_

See we have a new function called `on_bot_started` with a decorator `@bot.listen`? This is an event listener. Here, _Hikari_ is listening for the [`hikari.StartedEvent`](https://hikari-py.github.io/hikari/hikari/events/lifetime_events.html#hikari.events.lifetime_events.StartedEvent). This event is triggered whenever your bot gets online. So when your bot gets ready, it will print out "I am online". The function will take `event` as it's argument and it has all the properties of the event it was listening for e.g all the properties of the [`hikari.StartedEvent`](https://hikari-py.github.io/hikari/hikari/events/lifetime_events.html#hikari.events.lifetime_events.StartedEvent) class. Unlike _discord.py_, you can name your function anything you want. Since these functions are handling events, we will be referring to them as event handlers.

Let's make another event handler!

`bot.py`

```python

import lightbulb
import hikari

with open("./token.txt") as fp:
    TOKEN = fp.read().strip()

bot = lightbulb.Bot(token=TOKEN, prefix="b!")

@bot.listen(hikari.StartedEvent)
async def on_bot_started(event):
    """Prints out 'I am online' when the bot is ready to work"""
    print("I am online")

@bot.listen(hikari.MessageCreateEvent)
async def on_message(event):
    """Prints out 'I have been mentioned' when the someone mentions the bot in a message"""
    if bot.me.mention in in event.message.content:
        print("I have been mentioned")

# Never forget to leave this on the bottom of your code
# Anything after this line will only be executed once the bot logs out
bot.run()
```

Here, we have made a new event handler called `on_message`. This event handler is listening for the [`hikari.MessageCreateEvent`](https://hikari-py.github.io/hikari/hikari/events/message_events.html#hikari.events.message_events.MessageCreateEvent) which is triggered when someone sends a message to the bot's DM or any server the bot is in. Once the event has been triggered, we are checking if the bot was mentioned in the message. To access the message's content, we are using [`event.message.content`](https://hikari-py.github.io/hikari/hikari/events/message_events.html#hikari.events.message_events.MessageCreateEvent.message) which is a property of the [`hikari.MessageCreateEvent`](https://hikari-py.github.io/hikari/hikari/events/message_events.html#hikari.events.message_events.MessageCreateEvent) class. [`bot.me`](https://hikari-py.github.io/hikari/hikari/impl/bot.html#hikari.impl.bot.BotApp.me) is a [`hikari.OwnUser`](https://hikari-py.github.io/hikari/hikari/users.html#hikari.users.OwnUser) object which has a property called [`mention`](https://hikari-py.github.io/hikari/hikari/users.html#hikari.users.User.mention). So if the bot is mentioned in a message, it will print out "I have been mentioned" on the console.

---

Now that you have understood how events work and how to handle them with _Hikari_ and _Lightbulb_, it's time to learn how to make commands. This is where the fun part starts. Head over to the [**next section**](./creating-commands.md) and I'll teach you how commands work in _Hikari_ and _Lightbulb_!
