# Getting Started

Hi! So you made it here? Let's get back where we left off last time. So, to begin with using _Hikari_, you need to have Python installed already. If you don't know how to install it, you can check out this [**tutorial**](../faqs/installing-python.md).

## Installing Hikari

Once you have installed Python, it's time to install _Hikari_ so you can get started working on your bot. So open up your terminal or command prompt and type:

```bash
pip install hikari
```

## Making a basic bot

Once you have installed Hikari, make a new folder on your desktop or wherever you want, it is now time to start coding!

So first of all you need to make a text file called `token.txt` and paste your bot's token there. If you don't know how to get your bot's token, please refer to this [**guide**](../starting/making-the-bot.md).

Your bot's token might look like this

```
NTI2NTIxMzIyOTI3NDIzNDkx.XP_KqA.h7v_KI4P7Qwh1Lp5ZbIuSuJ7Ygs
```

_This token is obviously fake... But keep your bot's token **very very** private._

> **IMPORTANT:** Do not forget to add the token file to your `.gitignore` if you are using Git, as not doing so will most likely lead to you leaking the token which basically gives a free pass to anyone that wants to cause destruction.

Now make a `bot.py` file in the same folder as the token file. This is where you will be working on your bot. Now paste this code in the file:

```python
import hikari

with open("./token.txt") as fp:
    TOKEN = fp.read().strip()

bot = hikari.BotApp(token=TOKEN)

bot.run()
```

So what are we doing here? We are basically making a usable connection with the Discord API by creating a bot client. With this client you'll be able to receive events (such as a new message, new member on a guild or even an audit log event on a ban!) and send requests to the API in order to post messages, add reactions or kick someone!

At first, we are importing the _Hikari_ module. Then we are reading from the `token.txt` file that we made earlier and accessing the token from there. After that we made an instance of `hikari.BotApp` class with our token. This is the bot client for communicating with the Discord API. And at last, we are running the bot using `bot.run()`.

## Using lightbulb

There is a library called _lightbulb_ that extends _hikari_ and allows you to easily make commands, handle events, add plugins, etc. With this module, making bots with Hikari is ten times more easier! To install it, open your terminal and type:

```bash
pip install hikari-lightbulb
```

In your `bot.py` file, replace the existing code with this:

```python
import lightbulb

with open("./token.txt") as fp:
    TOKEN = fp.read().strip()

bot = lightbulb.Bot(token=TOKEN, prefix="b!")

bot.run()
```

As you can see, it's pretty straightforward. But we have to pass a new argument called `prefix`. A bot's prefix is a character or a sequence of characters that will trigger the bot's commands. For example, with the prefix shown in the above code-block, if you type `b!somecommand` then it will trigger a command called `somecommand` altough it doesn't exist right now since this is just an example. It's important to make that prefix unique as it may get really messy really fast on servers with a lot of bots.

So now, open your terminal and type:

```bash
python bot.py
```

Now your bot should become online.

Congratulations, you now have a working Discord bot!

Right now this bot doesn't do anything because we haven't coded any commands or event handlers for it. But we will be doing that in the next section. Anyways, I hope you enjoyed this one. See you in the [**next section**](./handling-events.md)!
