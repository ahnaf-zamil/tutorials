# Hikari Tutorial

**_by `Ahnaf#4346`_**

---

Welcome to the tutorial for the Python library, _Hikari_. _Hikari_ is a lightweight
and feature-rich module for making Discord bots in Python. If you ever wanted to make
Discord bots in Python, Hikari might be a good option for you since it is properly typed and tested. Hikari also has a sort of pluggable structure for its cache, so you can hook it to Redis and similar systems, but that's a feature that is mostly useful for big bots. It uses `asyncio` for concurrency so that your bot can be easily accessed everywhere at the same time without it being blocking. If you are feeling scared by hearing terms like `concurrency`, `async`, etc, don't worry, I will be guiding you through the entire tutorial on how you can create your own Discord Bot using Hikari. Once you have mastered the basics, you can add more functionality to your bot.

However, I highly suggest you know at least the basics of Python and object-oriented programming. You don't need to worry a lot about asynchronous programming because Hikari handles most of the stuff in the background.

If you are familiar with _discord.py_ then _Hikari_ would look familiar to you. But commands and plugins are some concepts that don't exist in _Hikari_ by default. These are the concepts from _Lightbulb_, which is an external library for _Hikari_ that is used for handling commands, plugins, etc, and that plugins are similar to cogs in _discord.py_ if you are confused.

In this tutorial, I will cover everything from the basics of _Hikari_ to more advanced concepts like commands, plugins, embeds, etc. to get you on track for becoming a bot developer. So start your text editor, grab a cup of coffee and follow along with me in this tutorial!

Once you are ready, just go to the next page by clicking [**Getting Started**](./getting-started.html).

## Helpful Resources

---

[Hikari Documentation](https://hikari-py.github.io/hikari/)

[Lightbulb Documentation](https://tandemdude.gitlab.io/lightbulb/)

[Python Documentation](https://docs.python.org/3/)
