# Creating Commands

So now that you've learnt how to handle Discord gateway events, let's talk about commands. Commands are a way that users can interact with a bot and that's what make bots fun. If you are still confused and don't know what commands are, let me explain. Commands are basically messages sent by users that start with your bot's prefix and have the command name after it. When these commands are triggered, the bot does something that is assigned to that command e.g replying with a message, updating a database etc.

To create a command, we can use our bot's [`command()`](https://tandemdude.gitlab.io/lightbulb/api-reference.html#lightbulb.command_handler.Bot.command) decorator. Let me show you how to do it.

`bot.py`

```python
@bot.command()
async def test(ctx):
    ...
```

Here, `bot` is an instance of `lightbulb.Bot`. You can now run this command using your bot's prefix and the command name like this `<bot-prefix>test`. By default, the command name will be the same as the function name, but you can change it by adding a name argument to the command decorator like this `@bot.command(name="awesomecommand")`. You can also add aliases to a command so that it can be triggered in multiple ways. You can set them like this `@bot.command(aliases=['testing', 'test123'])`.

Right now, the command does nothing so it's kinda boring. Let's add some functionality to the command and see what it does!

```python
@bot.command(name="test", aliases=["testing", "testing123"])
async def testing_command(ctx):
    await ctx.reply("Hello there! This is a test command.")
```

Now you can run this command by doing `<bot-prefix>test`or use any of the aliases instead of `test`. In this code, I made a command called `test` and added two aliases to it so that the user can trigger the command in multiple ways. In the command, I took `ctx` as the first argument which is the [`Context`](https://tandemdude.gitlab.io/lightbulb/api-reference.html#module-lightbulb.context) object (I will talk more about the context object later) and used `await ctx.reply()` to send a message in the same channel the command was triggered from.

> **Note:** The `ctx.reply()` method is a coroutine/asynchronous. That's why you need to use the `await` keyword before it.  
> **Also Note:** If you are from a `discord.py` background, `ctx.reply()` basically does the same thing as `ctx.send()`.

Now let's make a ping command which will return the bot's latency to Discord!

```python
@bot.command()
async def ping(ctx):
    latency = bot.heartbeat_latency * 1000 # Multiplying the latency with 1000 to convert them to milliseconds
    await ctx.reply(f"Pong! {latency}ms")
```

Here, we made a command called `ping`. This command creates a variable called latency which is the average gateway latency of each shard in milliseconds and then, replies to the command with a message containing the latency. You can run this command using the same way you run other commands (`<bot-prefix>ping`).

## The Context Object

The context is a special argument that's compulsary for all commands to take. This argument provides you with a [`Context`](https://tandemdude.gitlab.io/lightbulb/api-reference.html#module-lightbulb.context) object relating to information about the command e.g command author, channel, guild, message etc. All commands must take this argument.

---

Now that you know how to make commands in Hikari, I think you are ready do learn how to pass data to your commands using command arguments in the next [**section**](./command-arguments.md)!
